package org.cuentadealtocosto.cuentadealtocosto.network;

import org.cuentadealtocosto.cuentadealtocosto.entities.BaseResponse;
import org.cuentadealtocosto.cuentadealtocosto.entities.Categories;
import org.cuentadealtocosto.cuentadealtocosto.entities.Category;
import org.cuentadealtocosto.cuentadealtocosto.entities.CategoryDataRequest;
import org.cuentadealtocosto.cuentadealtocosto.entities.Parameters;
import org.cuentadealtocosto.cuentadealtocosto.entities.SessionData;
import org.cuentadealtocosto.cuentadealtocosto.entities.UserData;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by ronaldruiz on 5/14/18.
 */

public interface NetworkService {
    @GET("init/parameters")
    Observable<BaseResponse<Parameters>> getParameters();
    @POST("auth/login")
    Observable<BaseResponse<SessionData>> login(@Body UserData userData);
    @POST("operations/getCategories")
    Observable<BaseResponse<Categories>> getMenuData(@Body CategoryDataRequest sessionId);
}

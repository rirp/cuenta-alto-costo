package org.cuentadealtocosto.cuentadealtocosto.di;

import android.os.Handler;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ronaldruiz on 5/9/18.
 */

@Module
public class HandlerModule {
    @Provides
    static Handler provideHandler() {
        return new Handler();
    }
}

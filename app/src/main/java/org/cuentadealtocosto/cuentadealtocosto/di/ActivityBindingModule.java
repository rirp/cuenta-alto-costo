package org.cuentadealtocosto.cuentadealtocosto.di;

import org.cuentadealtocosto.cuentadealtocosto.ui.ContentActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.MainActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.MainActivityModule;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.view.LoadingActivity;
import org.cuentadealtocosto.cuentadealtocosto.di.scopes.PerActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.view.LoadingActivityModule;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.view.LoginActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.view.LoginActivityModule;

import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ronaldruiz on 5/9/18.
 */
@Module(includes = AndroidInjectionModule.class)
public interface ActivityBindingModule {
    @PerActivity
    @ContributesAndroidInjector(modules = {LoadingActivityModule.class})
    LoadingActivity loadingActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    LoginActivity loginActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    MainActivity categoryActivityInbjector();

    @PerActivity
    @ContributesAndroidInjector
    ContentActivity contentActivityInjector();
}

package org.cuentadealtocosto.cuentadealtocosto.di.app;

import android.app.Application;

import org.cuentadealtocosto.cuentadealtocosto.di.ActivityBindingModule;
import org.cuentadealtocosto.cuentadealtocosto.di.HandlerModule;
import org.cuentadealtocosto.cuentadealtocosto.network.NetworkModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by ronaldruiz on 5/9/18.
 */
@Singleton
@Component(modules = {AppModule.class,
        AndroidSupportInjectionModule.class,
        ActivityBindingModule.class,
        NetworkModule.class,
        HandlerModule.class})
interface AppComponent extends AndroidInjector<DaggerApplication> {
    void inject(App app);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}


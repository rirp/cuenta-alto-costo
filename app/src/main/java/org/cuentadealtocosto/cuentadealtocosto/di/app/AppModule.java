package org.cuentadealtocosto.cuentadealtocosto.di.app;

import org.cuentadealtocosto.cuentadealtocosto.entities.SessionData;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;

/**
 * Created by ronaldruiz on 5/9/18.
 */

@Module(includes = AndroidInjectionModule.class)
public class AppModule {
    @Provides
    @Singleton
    static SessionData sessionDataInject() {
        return new SessionData();
    }
}

package org.cuentadealtocosto.cuentadealtocosto.utils;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public class Constants {
    public interface Network {
        final class StatusCode {
            public static final Short SUCCESSFUL = 0;
            public static final Short GENERAL_ERROR = -1;
        }
        final class HttpCode {
            public static final Integer BAD_REQUEST = 400;
            public static final Integer INTERNAL_ERROR = 500;
        }
    }
}

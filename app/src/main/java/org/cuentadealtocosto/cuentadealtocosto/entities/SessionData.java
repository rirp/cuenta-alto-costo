package org.cuentadealtocosto.cuentadealtocosto.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ronaldruiz on 5/27/18.
 */

public class SessionData {
    @SerializedName("name")
    private String name;
    @SerializedName("sessionId")
    private String sessionId;

    public void setData(SessionData sessionData) {
        this.setName(sessionData.getName());
        this.setSessionId(sessionData.getSessionId());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}

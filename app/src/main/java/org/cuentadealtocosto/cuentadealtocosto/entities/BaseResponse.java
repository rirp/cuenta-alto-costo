package org.cuentadealtocosto.cuentadealtocosto.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ronaldruiz on 5/12/18.
 */

public class BaseResponse<T> {
    @SerializedName("errorCode")
    private Short errorCode;
    @SerializedName("serviceName")
    private String serviceName;
    @SerializedName("message")
    private String message;
    @SerializedName("response")
    private T response;

    public Short getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Short errorCode) {
        this.errorCode = errorCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}

package org.cuentadealtocosto.cuentadealtocosto.entities;

import com.google.gson.annotations.SerializedName;

import javax.inject.Singleton;

/**
 * Created by ronaldruiz on 5/27/18.
 */
@Singleton
public class Parameters {
    @SerializedName("registerURL")
    private String registerURL;

    public String getRegisterURL() {
        return registerURL;
    }

    public void setRegisterURL(String registerURL) {
        this.registerURL = registerURL;
    }
}

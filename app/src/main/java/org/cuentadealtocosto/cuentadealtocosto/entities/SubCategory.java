package org.cuentadealtocosto.cuentadealtocosto.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ronaldruiz on 5/17/18.
 */

public class SubCategory {
    @SerializedName("id")
    private Integer id;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

package org.cuentadealtocosto.cuentadealtocosto.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ronaldruiz on 5/29/18.
 */

public class UserData {
    @SerializedName("username")
    private String username;
    @SerializedName("pin")
    private String pin;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}

package org.cuentadealtocosto.cuentadealtocosto.entities;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

/**
 * Created by ronaldruiz on 5/29/18.
 */

public class Categories {
    @SerializedName("categories")
    private List<Category> categoryList;

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categories) {
        if (categories == null) {
            this.categoryList = Collections.emptyList();
        }
        this.categoryList = categories;
    }
}

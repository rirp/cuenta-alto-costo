package org.cuentadealtocosto.cuentadealtocosto.ui.main.model;

/**
 * Created by ronaldruiz on 5/23/18.
 */

public interface MainModel {
    void getCategories(String sessionId, MainModelCallback callback);
}

package org.cuentadealtocosto.cuentadealtocosto.ui.login.presenter;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public interface LoginPresenter {
    void login(String username, String password);
}

package org.cuentadealtocosto.cuentadealtocosto.ui.loading.model;

/**
 * Created by ronaldruiz on 5/14/18.
 */

public interface LoadingModelCallback {
    void onGetParametersSuccess();
    void onGetParametersFail();
}

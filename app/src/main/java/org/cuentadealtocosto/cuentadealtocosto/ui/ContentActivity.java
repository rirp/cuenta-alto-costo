package org.cuentadealtocosto.cuentadealtocosto.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.cuentadealtocosto.cuentadealtocosto.R;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContentActivity extends BaseActivity {
    private static final int FINISHED_PERCENTAGE = 100;
    @BindView(R.id.webView_content)
    protected WebView webView;
    @BindView(R.id.layout_text_container)
    protected LinearLayout layoutTextContainer;
    @BindView(R.id.iv_logo_header)
    protected ImageView ivLogoHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        ButterKnife.bind(this);
        layoutTextContainer.setVisibility(View.GONE);
        ivLogoHeader.setImageResource(R.drawable.logo);
        String url = "";
        if (getIntent().getStringExtra("url") != null) {
            url = getIntent().getStringExtra("url");
        }
        loadContent(this, url);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void loadContent(final Context context, String url) {
        showLoading();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebClient());
        webView.loadUrl(url);
    }

    private class WebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (view.getProgress() == FINISHED_PERCENTAGE) {
                removeLoading();
            }
        }
    }
}

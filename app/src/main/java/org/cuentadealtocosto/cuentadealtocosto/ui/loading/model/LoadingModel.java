package org.cuentadealtocosto.cuentadealtocosto.ui.loading.model;

/**
 * Created by ronaldruiz on 5/12/18.
 */

public interface LoadingModel {
    void consumeGetParameters(LoadingModelCallback callback);
}

package org.cuentadealtocosto.cuentadealtocosto.ui.login.view;

import org.cuentadealtocosto.cuentadealtocosto.di.scopes.PerActivity;
import org.cuentadealtocosto.cuentadealtocosto.entities.SessionData;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.model.LoginModel;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.model.LoginModelImp;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.presenter.LoginPresenter;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.presenter.LoginPresenterImp;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 5/10/18.
 */
@Module
public abstract class LoginActivityModule {
    @Binds
    @PerActivity
    abstract LoginView view(LoginActivity activity);

    @Binds
    @PerActivity
    abstract LoginPresenter loginPresenterInject(LoginPresenterImp presenter);

    @Provides
    @PerActivity
    static LoginModel loginModelInjector(Retrofit retrofit, SessionData sessionData) {
        return new LoginModelImp(retrofit, sessionData);
    }
}

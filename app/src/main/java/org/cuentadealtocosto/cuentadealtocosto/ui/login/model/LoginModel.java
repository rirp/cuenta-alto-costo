package org.cuentadealtocosto.cuentadealtocosto.ui.login.model;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public interface LoginModel {
    void login(String username, String password, LoginModelCallback callback);
}

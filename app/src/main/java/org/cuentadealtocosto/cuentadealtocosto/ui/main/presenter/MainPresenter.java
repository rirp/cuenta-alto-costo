package org.cuentadealtocosto.cuentadealtocosto.ui.main.presenter;

/**
 * Created by ronaldruiz on 5/23/18.
 */

public interface MainPresenter {
    void getMenuData(String sessionId);
}

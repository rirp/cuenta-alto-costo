package org.cuentadealtocosto.cuentadealtocosto.ui.loading.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import org.cuentadealtocosto.cuentadealtocosto.R;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.presenter.LoadingPresenter;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.view.LoginActivity;

import javax.inject.Inject;

public class LoadingActivity extends BaseActivity implements LoadingView {
    @Inject
    LoadingPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        presenter.getParameters();
    }

    @Override
    public void goToLogin() {
        this.removeLoading();
        startActivity(LoginActivity.class);
    }

    @Override
    public void showError() {
        this.removeLoading();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error")
                .setMessage("Ha ocurrido un error en la comunicación.")
                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}

package org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu;

/**
 * Created by ronaldruiz on 5/17/18.
 */

public interface MainAdapterListener {
    void loadWebPage(String url);
}

package org.cuentadealtocosto.cuentadealtocosto.ui.main.model;

import org.cuentadealtocosto.cuentadealtocosto.entities.BaseResponse;
import org.cuentadealtocosto.cuentadealtocosto.entities.Categories;
import org.cuentadealtocosto.cuentadealtocosto.entities.CategoryDataRequest;
import org.cuentadealtocosto.cuentadealtocosto.network.NetworkService;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;

import static org.cuentadealtocosto.cuentadealtocosto.utils.Constants.Network.HttpCode.BAD_REQUEST;
import static org.cuentadealtocosto.cuentadealtocosto.utils.Constants.Network.HttpCode.INTERNAL_ERROR;

/**
 * Created by ronaldruiz on 5/23/18.
 */

public class MainModelImp implements MainModel, BaseModel<BaseResponse<Categories>> {
    private Retrofit retrofit;
    private MainModelCallback callback;
    private String sessionId;

    public MainModelImp(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void getCategories(String sessionId, MainModelCallback callback) {
        this.callback = callback;
        this.sessionId =  sessionId;
        getObservable().subscribeWith(getObserver());
    }

    @Override
    public DisposableObserver<BaseResponse<Categories>> getObserver() {
        return new DisposableObserver<BaseResponse<Categories>>() {

            @Override
            public void onNext(BaseResponse<Categories> listBaseResponse) {
                if (listBaseResponse.getResponse() != null) {
                    callback.getCategoriesSuccessful(listBaseResponse.getResponse().getCategoryList());
                } else {
                    callback.getCategoriesFailure(listBaseResponse.getMessage());
                }
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof HttpException) {
                    callback.getCategoriesFailure("Error general en el sistema, intente más tarde");
                } else {
                    callback.getCategoriesFailure("Sin conexión a internet");
                }
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public Observable<BaseResponse<Categories>> getObservable() {
        CategoryDataRequest data = new CategoryDataRequest();
        data.setSessionId(sessionId);
        return retrofit.create(NetworkService.class)
                .getMenuData(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

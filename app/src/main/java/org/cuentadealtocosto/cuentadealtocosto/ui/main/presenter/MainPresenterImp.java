package org.cuentadealtocosto.cuentadealtocosto.ui.main.presenter;

import org.cuentadealtocosto.cuentadealtocosto.entities.Category;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.model.MainModel;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.model.MainModelCallback;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by ronaldruiz on 5/23/18.
 */

public class MainPresenterImp implements MainPresenter, MainModelCallback {
    private org.cuentadealtocosto.cuentadealtocosto.ui.main.view.MainView view;
    private MainModel model;

    @Inject
    public MainPresenterImp(org.cuentadealtocosto.cuentadealtocosto.ui.main.view.MainView view, MainModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void getMenuData(String sessionId) {
        model.getCategories(sessionId,this);
    }

    @Override
    public void getCategoriesSuccessful(List<Category> categories) {
        view.setCategories(categories);
    }

    @Override
    public void getCategoriesFailure(String message) {
        view.showErrorAtGetCategory(message);
    }
}

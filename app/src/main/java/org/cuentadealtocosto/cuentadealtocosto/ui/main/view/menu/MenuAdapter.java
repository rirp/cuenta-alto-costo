package org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import org.cuentadealtocosto.cuentadealtocosto.R;
import org.cuentadealtocosto.cuentadealtocosto.entities.Category;
import org.cuentadealtocosto.cuentadealtocosto.entities.SubCategory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by ronaldruiz on 5/12/18.
 */

public class MenuAdapter extends RecyclerView.Adapter<MainHolder> implements Filterable {
    private MainAdapterListener mClickListener;
    private List<Category> mCategories;
    private List<Category> mCategoriesFilter;
    private CategoryFilter categoryFilter;

    @Inject
    public MenuAdapter(MainAdapterListener clickListener) {
        this.mClickListener = clickListener;
    }

    public void setCategories(List<Category> categories) {
        this.mCategories = categories;
        this.mCategoriesFilter = categories;
    }

    @NonNull
    @Override
    public MainHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category_subcategory_listing, parent, false);
        return new MainHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MainHolder holder, int position) {
        Category category = mCategoriesFilter.get(position);
        holder.setData(category, mClickListener);
    }

    @Override
    public int getItemCount() {
        if (mCategoriesFilter == null) {
            return 0;
        }
        return mCategoriesFilter.size();
    }

    @Override
    public Filter getFilter() {
        if (categoryFilter == null) {
            categoryFilter = new CategoryFilter();
        }
        return categoryFilter;
    }

    public class CategoryFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            FilterResults results = new FilterResults();
            if (charString.isEmpty()) {
                results.count = mCategories.size();
                results.values = mCategories;
            } else {
                List<Category> filterList = new ArrayList<>();
                for (Category category : mCategoriesFilter) {
                    for (SubCategory subCategory : category.getSubCategoryList()) {
                        if ((subCategory.getTitle().toUpperCase()).contains(charString.toUpperCase())) {
                            if (filterList.size() == 0) {
                                filterList.add(mockCategory());
                            }
                            filterList.get(0).getSubCategoryList().add(subCategory);
                        }
                    }
                }
                mCategoriesFilter = filterList;
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            mCategoriesFilter = (List<Category>) filterResults.values;
            notifyDataSetChanged();
        }

        private Category mockCategory() {
            Category category = new Category();
            category.setId(-53021);
            category.setTitle("");
            category.setSubCategoryList(new ArrayList<SubCategory>());
            return category;
        }
    }
}

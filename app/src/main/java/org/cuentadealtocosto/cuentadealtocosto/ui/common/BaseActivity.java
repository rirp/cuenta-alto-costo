package org.cuentadealtocosto.cuentadealtocosto.ui.common;


import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import android.widget.Toast;

import org.cuentadealtocosto.cuentadealtocosto.R;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

@SuppressLint("Registered")
public class BaseActivity extends DaggerAppCompatActivity {
    private ProgressDialog mLoadingDialog;
    private static final long TIME_OUT = 3000;
    @Inject
    Handler timeoutHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        hideStatusBar();
        super.onCreate(savedInstanceState);
    }

    private void hideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private Runnable loadingTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            removeLoading();
            postRemoveLoading();
        }
    };

    public void showLoading() {
        if (this.mLoadingDialog == null || !this.mLoadingDialog.isShowing()) {
            this.mLoadingDialog = ProgressDialog
                    .show(this,
                            "",
                            this.getString(R.string.string_loading),
                            true,
                            false,
                            null);
            //timeoutHandler.postDelayed(loadingTimeoutRunnable,  TIME_OUT);
        }
    }

    public void removeLoading() {
        try {
            if (this.mLoadingDialog != null && this.mLoadingDialog.isShowing()) {
                //timeoutHandler.removeCallbacks(loadingTimeoutRunnable);
                this.mLoadingDialog.dismiss();
                this.mLoadingDialog = null;
            }
        } catch (final IllegalArgumentException e) {
            this.mLoadingDialog = null;
        }
    }

    public void startActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
        finish();
    }

    public void postRemoveLoading() {

    }

    public void showAlert(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}

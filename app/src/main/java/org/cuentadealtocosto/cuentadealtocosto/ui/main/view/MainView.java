package org.cuentadealtocosto.cuentadealtocosto.ui.main.view;

import org.cuentadealtocosto.cuentadealtocosto.entities.Category;

import java.util.List;

/**
 * Created by ronaldruiz on 5/23/18.
 */

public interface MainView {
    void setCategories(List<Category> categoryList);
    void showErrorAtGetCategory(String message);
}

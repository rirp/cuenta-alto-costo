package org.cuentadealtocosto.cuentadealtocosto.ui.common;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by ronaldruiz on 5/15/18.
 */

public interface BaseModel<O> {
    DisposableObserver<O> getObserver();
    Observable<O> getObservable();
}

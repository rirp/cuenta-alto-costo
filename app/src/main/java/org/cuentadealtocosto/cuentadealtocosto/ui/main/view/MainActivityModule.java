package org.cuentadealtocosto.cuentadealtocosto.ui.main.view;

import org.cuentadealtocosto.cuentadealtocosto.di.scopes.PerActivity;
import org.cuentadealtocosto.cuentadealtocosto.entities.SessionData;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.model.MainModel;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.model.MainModelImp;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.presenter.MainPresenter;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.presenter.MainPresenterImp;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu.MainAdapterModule;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 5/10/18.
 */
@Module(includes = MainAdapterModule.class)
public abstract class MainActivityModule {

    @Binds
    @PerActivity
    abstract MainView view(MainActivity activity);

    @Binds
    @PerActivity
    abstract MainPresenter presenter(MainPresenterImp presenterImp);

    @Provides
    @PerActivity
    static MainModel mainModelInjector(Retrofit retrofit) {
        return new MainModelImp(retrofit);
    }
}

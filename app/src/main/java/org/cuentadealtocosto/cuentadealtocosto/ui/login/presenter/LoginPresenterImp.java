package org.cuentadealtocosto.cuentadealtocosto.ui.login.presenter;

import org.cuentadealtocosto.cuentadealtocosto.ui.login.model.LoginModel;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.model.LoginModelCallback;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.view.LoginView;

import javax.inject.Inject;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public class LoginPresenterImp implements LoginPresenter, LoginModelCallback {
    private LoginView view;
    private LoginModel model;

    @Inject
    public LoginPresenterImp(LoginView view, LoginModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void login(String username, String password) {
        model.login(username, password, this);
    }

    @Override
    public void onLoginSuccess() {
        view.loginSuccess();
    }

    @Override
    public void onLoginFail(String message) {
        view.loginFail(message);
    }
}

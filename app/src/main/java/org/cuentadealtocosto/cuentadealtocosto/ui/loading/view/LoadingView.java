package org.cuentadealtocosto.cuentadealtocosto.ui.loading.view;

/**
 * Created by ronaldruiz on 5/9/18.
 */

public interface LoadingView {
    void goToLogin();
    void showError();
}

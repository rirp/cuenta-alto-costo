package org.cuentadealtocosto.cuentadealtocosto.ui.main.model;

import org.cuentadealtocosto.cuentadealtocosto.entities.Category;

import java.util.List;

/**
 * Created by ronaldruiz on 5/23/18.
 */

public interface MainModelCallback {
    void getCategoriesSuccessful(List<Category> categories);
    void getCategoriesFailure(String message);
}

package org.cuentadealtocosto.cuentadealtocosto.ui.login.model;

import org.cuentadealtocosto.cuentadealtocosto.entities.BaseResponse;
import org.cuentadealtocosto.cuentadealtocosto.entities.SessionData;
import org.cuentadealtocosto.cuentadealtocosto.entities.UserData;
import org.cuentadealtocosto.cuentadealtocosto.network.NetworkService;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseModel;
import org.cuentadealtocosto.cuentadealtocosto.utils.Constants;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;

import static org.cuentadealtocosto.cuentadealtocosto.utils.Constants.Network.HttpCode.BAD_REQUEST;
import static org.cuentadealtocosto.cuentadealtocosto.utils.Constants.Network.HttpCode.INTERNAL_ERROR;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public class LoginModelImp implements LoginModel, BaseModel<BaseResponse<SessionData>> {
    private final Retrofit retrofit;
    private LoginModelCallback callback;
    private UserData userData;

    private SessionData sessionData;

    public LoginModelImp(Retrofit retrofit, SessionData sessionData) {
        this.retrofit = retrofit;
        this.sessionData = sessionData;
    }

    @Override
    public void login(String username, String password, LoginModelCallback callback) {
        this.callback = callback;
        this.userData = new UserData();
        this.userData.setUsername(username);
        this.userData.setPin(password);
        getObservable().subscribeWith(getObserver());
    }

    @Override
    public DisposableObserver<BaseResponse<SessionData>> getObserver() {
        return new DisposableObserver<BaseResponse<SessionData>>() {

            @Override
            public void onNext(BaseResponse<SessionData> sessionDataBaseResponse) {
                if (sessionDataBaseResponse.getErrorCode() == Constants.Network.StatusCode.SUCCESSFUL) {
                    sessionData.setData(sessionDataBaseResponse.getResponse());
                    callback.onLoginSuccess();
                } else {
                    callback.onLoginFail("Ha ocurrido un error, intente otra vez");
                }
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof HttpException) {
                    Integer code = ((HttpException) e).code();
                    if (code >= BAD_REQUEST && code < INTERNAL_ERROR) {
                        callback.onLoginFail("Usuario o contraseña inválido");
                    } else {
                        callback.onLoginFail("Error general en el sistema, intente más tarde");
                    }
                } else {
                    callback.onLoginFail("Sin conexión a internet");
                }
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public Observable<BaseResponse<SessionData>> getObservable() {
        return retrofit.create(NetworkService.class)
                .login(userData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

package org.cuentadealtocosto.cuentadealtocosto.ui.login.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import org.cuentadealtocosto.cuentadealtocosto.R;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.login.presenter.LoginPresenter;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via tfUsername/tfPassword.
 */
public class LoginActivity extends BaseActivity implements LoginView, TextWatcher {
    private static final String CREATE_ACCOUNT_URL = "https://cuentadealtocosto.org/site/index.php/registro-usuarios-publicos";

    @Inject
    LoginPresenter presenter;

    @BindView(R.id.textfield_username)
    AutoCompleteTextView tfUsername;
    @BindView(R.id.textfield_password)
    EditText tfPassword;
    @BindView(R.id.button_sign_in)
    Button btnSignIn;
    @BindView(R.id.button_create_account)
    Button buttonCreateAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        tfUsername.addTextChangedListener(this);
        tfPassword.addTextChangedListener(this);
    }

    @Override
    public void loginSuccess() {
        removeLoading();
        this.startActivity(MainActivity.class);
    }

    @Override
    public void loginFail(String message) {
        removeLoading();
        this.showAlert(message);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        boolean enable = (tfPassword.getText().toString().trim().length() >= 4) &&
                (tfUsername.getText().toString().trim().length() >= 5);
        btnSignIn.setEnabled(enable);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @OnClick(R.id.button_sign_in)
    public void onButtonSignInClicked() {
        showLoading();
        presenter.login(tfUsername.getText().toString(), tfPassword.getText().toString());
    }

    @OnClick(R.id.button_create_account)
    public void onButtonCreateAccountClicked() {
        Uri webpage = Uri.parse(CREATE_ACCOUNT_URL);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }else{
            showAlert(getString(R.string.string_page_not_found));
        }
    }
}


package org.cuentadealtocosto.cuentadealtocosto.ui.loading.presenter;

/**
 * Created by ronaldruiz on 5/12/18.
 */

public interface LoadingPresenter {
    void getParameters();
}

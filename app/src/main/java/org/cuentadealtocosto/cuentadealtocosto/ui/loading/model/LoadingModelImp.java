package org.cuentadealtocosto.cuentadealtocosto.ui.loading.model;

import org.cuentadealtocosto.cuentadealtocosto.entities.BaseResponse;
import org.cuentadealtocosto.cuentadealtocosto.entities.Parameters;
import org.cuentadealtocosto.cuentadealtocosto.network.NetworkService;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 5/12/18.
 */

public class LoadingModelImp implements LoadingModel, BaseModel<BaseResponse<Parameters>> {
    private LoadingModelCallback callback;
    private Retrofit retrofit;

    public LoadingModelImp(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public void consumeGetParameters(LoadingModelCallback callback) {
        this.callback = callback;
        getParameters();
    }

    private void getParameters() {
        getObservable().subscribeWith(getObserver());
    }

    @Override
    public DisposableObserver<BaseResponse<Parameters>> getObserver() {
        return new DisposableObserver<BaseResponse<Parameters>>() {

            @Override
            public void onNext(BaseResponse<Parameters> parametersBaseResponse) {
                callback.onGetParametersSuccess();
            }

            @Override
            public void onError(Throwable e) {
                callback.onGetParametersFail();
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public Observable<BaseResponse<Parameters>> getObservable() {
        return retrofit.create(NetworkService.class)
                .getParameters()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

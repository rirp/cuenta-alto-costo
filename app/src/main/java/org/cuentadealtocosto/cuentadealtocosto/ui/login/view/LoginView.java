package org.cuentadealtocosto.cuentadealtocosto.ui.login.view;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public interface LoginView {
    void loginSuccess();
    void loginFail(String message);
}

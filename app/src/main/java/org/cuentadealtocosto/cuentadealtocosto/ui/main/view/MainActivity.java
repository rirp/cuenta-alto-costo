package org.cuentadealtocosto.cuentadealtocosto.ui.main.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;
import android.widget.TextView;

import org.cuentadealtocosto.cuentadealtocosto.R;
import org.cuentadealtocosto.cuentadealtocosto.entities.Category;
import org.cuentadealtocosto.cuentadealtocosto.entities.SessionData;
import org.cuentadealtocosto.cuentadealtocosto.ui.common.BaseActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.presenter.MainPresenter;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu.MainAdapterListener;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu.MenuAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainAdapterListener, MainView, SearchView.OnQueryTextListener {
    @Inject
    protected MainPresenter presenter;
    @Inject
    protected SessionData sessionData;
    @Inject
    protected MenuAdapter adapter;
    @BindView(R.id.recycler_view)
    protected RecyclerView rvMenu;
    @BindView(R.id.search_menu)
    protected SearchView searchMenu;
    @BindView(R.id.tvUserName)
    protected TextView tvUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        rvMenu.setLayoutManager(new LinearLayoutManager(this));
        rvMenu.setAdapter(adapter);
        searchMenu.setOnQueryTextListener(this);
        tvUserName.setText(sessionData.getName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        presenter.getMenuData(sessionData.getSessionId());
    }

    @Override
    public void setCategories(List<Category> categoryList) {
        removeLoading();
        adapter.setCategories(categoryList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorAtGetCategory(String message) {
        removeLoading();
        showAlert(message);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.getFilter().filter(s);
        return false;
    }

    @Override
    public void loadWebPage(String url) {

    }
}

package org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu;

import org.cuentadealtocosto.cuentadealtocosto.di.scopes.PerActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.main.view.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by ronaldruiz on 5/17/18.
 */
@Module
public abstract class MainAdapterModule {
    @Binds
    @PerActivity
    public abstract MainAdapterListener mainAdapterListenerInjector(MainActivity mainActivity);

    @Provides
    @PerActivity
    static MenuAdapter menuAdapterInjector(MainAdapterListener adapterListener) {
        return new MenuAdapter(adapterListener);
    }
}

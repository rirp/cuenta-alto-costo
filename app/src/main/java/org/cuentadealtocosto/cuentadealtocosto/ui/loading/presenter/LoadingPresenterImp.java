package org.cuentadealtocosto.cuentadealtocosto.ui.loading.presenter;

import org.cuentadealtocosto.cuentadealtocosto.ui.loading.model.LoadingModel;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.model.LoadingModelCallback;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.view.LoadingView;

import javax.inject.Inject;

/**
 * Created by ronaldruiz on 5/12/18.
 */

public class LoadingPresenterImp implements LoadingPresenter, LoadingModelCallback {
    private LoadingView view;
    private LoadingModel model;

    @Inject
    public LoadingPresenterImp(LoadingView view, LoadingModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void getParameters() {
        this.model.consumeGetParameters(this);
    }

    @Override
    public void onGetParametersSuccess() {
        this.view.goToLogin();
    }

    @Override
    public void onGetParametersFail() {
        this.view.showError();
    }
}

package org.cuentadealtocosto.cuentadealtocosto.ui.loading.view;

import org.cuentadealtocosto.cuentadealtocosto.di.scopes.PerActivity;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.model.LoadingModel;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.model.LoadingModelImp;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.presenter.LoadingPresenter;
import org.cuentadealtocosto.cuentadealtocosto.ui.loading.presenter.LoadingPresenterImp;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 5/9/18.
 */
@Module
public abstract class LoadingActivityModule {
    @Binds
    @PerActivity
    abstract LoadingView loadingView(LoadingActivity activity);

    @Binds
    @PerActivity
    abstract LoadingPresenter loadingPresenterInjector(LoadingPresenterImp presenterImp);

    @Provides
    static LoadingModel loadingModelInjector(Retrofit retrofit) {
        return new LoadingModelImp(retrofit);
    }
}

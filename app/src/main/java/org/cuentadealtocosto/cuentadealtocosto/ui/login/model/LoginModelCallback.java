package org.cuentadealtocosto.cuentadealtocosto.ui.login.model;

/**
 * Created by ronaldruiz on 5/10/18.
 */

public interface LoginModelCallback {
    void onLoginSuccess();
    void onLoginFail(String message);
}

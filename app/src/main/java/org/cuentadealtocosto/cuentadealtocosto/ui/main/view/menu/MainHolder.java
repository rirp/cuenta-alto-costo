package org.cuentadealtocosto.cuentadealtocosto.ui.main.view.menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cuentadealtocosto.cuentadealtocosto.R;
import org.cuentadealtocosto.cuentadealtocosto.entities.Category;
import org.cuentadealtocosto.cuentadealtocosto.entities.SubCategory;
import org.cuentadealtocosto.cuentadealtocosto.ui.ContentActivity;
import org.cuentadealtocosto.cuentadealtocosto.utils.AnimationHelper;

/**
 * Created by ronaldruiz on 5/17/18.
 */

public class MainHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static Integer TIME_ANIMATION = 500;
    private Context context;
    private LinearLayout llSubCategoryItems;
    private TextView tvCategoryName;
    private ImageView ivCategoryArrow;
    private MainAdapterListener mClickListener;

    public MainHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        llSubCategoryItems = itemView.findViewById(R.id.ll_sub_category_items);
        tvCategoryName = itemView.findViewById(R.id.tv_categoryName);
        ivCategoryArrow = itemView.findViewById(R.id.iv_category_arrow);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setData(Category category, MainAdapterListener mClickListener) {
        llSubCategoryItems.removeAllViews();
        tvCategoryName.setText(category.getTitle());
        tvCategoryName.setOnClickListener(this);
        for (final SubCategory subCategory : category.getSubCategoryList()) {
            TextView textView = new TextView(context);
            textView.setId(subCategory.getId());
            textView.setPadding(0, 20, 0, 20);
            textView.setGravity(Gravity.CENTER);
            textView.setText(subCategory.getTitle());
            textView.setTextColor(Color.WHITE);
            textView.setBackground(context.getDrawable(R.drawable.bg_sub_category));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 1, 0, 1);
            llSubCategoryItems.addView(textView, layoutParams);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ContentActivity.class);
                    intent.putExtra("url", subCategory.getUrl());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tv_categoryName) {
            if (llSubCategoryItems.getVisibility() == View.VISIBLE) {
                AnimationHelper.expandView(llSubCategoryItems, TIME_ANIMATION);
                ivCategoryArrow.setImageResource(android.R.drawable.arrow_down_float);
            } else {
                AnimationHelper.collapse(llSubCategoryItems, TIME_ANIMATION);
                ivCategoryArrow.setImageResource(android.R.drawable.arrow_up_float);
            }
        }
    }
}
